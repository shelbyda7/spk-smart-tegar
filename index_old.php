<?php 
include "config.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Welcome</title>
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="#page-top">Sistem Informasi Kelas Unggulan</a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto my-2 my-lg-0">
                        <li class="nav-item"><a class="nav-link" href="#home">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="#tentang">Tentang</a></li>
                        <li class="nav-item"><a class="nav-link" href="#informasi">Informasi</a></li>
                        <li class="nav-item"><a class="nav-link" href="#kegiatan">Kegiatan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#daftar">Daftar</a></li>
                        <li class="nav-item"><a class="nav-link" href="login.php">Sign Up</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- About-->
        <section class="page-section bg-info" id="home">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 class="text-white mt-0">Sistem Informasi Kelas Unggulan Metode SMART</h2>
                        <hr class="divider divider-light" />
                    </div>
                </div>
            </div>
        </section>
        <!-- Services-->
        <section class="page-section" id="tentang">
            <div class="container px-4 px-lg-5">
                <h2 class="text-center mt-0">Tentang</h2>
                <hr class="divider" />
                <div class="row gx-4 gx-lg-5">
                    <div class="col-lg-12 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-globe fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">Program Pengayaan</h3>
                            <p class="text-muted mb-0">
                            Beranjak dari tekad dan itikad melahirkan pembisaan mengaji itulah kemulaian
                            Kelas Pengayaan PAI diselenggarakan. Setelah masa dua tahun mencari pola yang
                            ideal sejak diluncurkan sebagai ‘branding proggramme service’ SMP Negeri 21 kota
                            Tangerang Selatan ini baru di tahun ke-3 model dan konten pembelajarannya mulai
                            bisa didefinifkan. Model kurikulumnya dinamai BASMALAH-Qu yang merupakan akronim
                            dari ‘Biasa Membaca, Menghafal, dan Menerjemah Al-Qur’an’. Model kurikulum ini
                            dirumuskan bukan tanpa alasan. Selain dorongan profetik agar ikhtiar pembelajaran
                            diarahkan untuk memudahkan peserta didik, pesan-pesan agung dalam Al-Qur’an, maupun
                            tausiah profetik yang disampaikan Rasulullah Saw.menyiratkan pesan singkat bahwa
                            berpusat pada Al-Qur’an lah pembelajaran inti agama Islam ini dituangkan. Karenanya,
                            pembelajaran paling mendasar yang harus diutamakan kepada peserta didik adalah
                            konten-konten yang didekat-dekatkan dengan pemahaman terhadap Al-Qur’an.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-globe fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">Fungsi & Tujuan</h3>
                            <p class="text-muted mb-0">
                            Kelas Pengayaan PAI adalah program pendidikan nonformal yang diselenggarakan pada
                            sekolah umum dalam hal ini SMP Negeri 21 Kota Tangerang Selatan yang berfungsi:
                            <p class="text-muted mb-0">a.Memenuhi Kebutuhan Masyarakat akan tambahan pendidikan
                                agama Islam khususnya pendidikan Al-Qur’an bagi peserta didik yang belajar di
                                SMP Negeri 21 Kota Tangerang Selatan,
                            <p class="text-muted mb-0">b.Mempersiapkan Peserta didik menjadi anggota masyarakat
                                yang memahami dan mengamalkan nilai ajaran agama islam.
                            <p class="text-muted mb-0">Adapun Tujuannya adalah Terbentuknya peserta didik yang memahami dan mengamalkan
                            nilai ajaran agama islam dan terbiasa membaca, menghafal dan menerjemahkan Al-Qur’an
                            dalam rangka mencerdaskan kehidupan bangsa yang beriman, bertakwa, dan berakhlak mulia
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-heart fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">Visi Misi</h3>
                            <p class="text-muted mb-0">
                            Mencerdaskan kehidupan bangsa yang beriman, bertakwa, dan berakhlak mulia.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section" id="informasi">
            <div class="container px-4 px-lg-5">
                <h2 class="text-center mt-0">Informasi Pengumuman Kelulusan</h2>
                <hr class="divider" />
                <div class="row gx-4 gx-lg-5">
                    <div class="col-lg-12 col-md-6 text-center">
                        <div class="mt-5">
                            <!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Hasil</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th scope="col">No</th>
						<th scope="col">Nama</th>
						<th scope="col">Kelas</th>
						<th scope="col">Hasil</th>
					</tr>
				</thead>
				<tbody>
					<?php
					// Menampilkan list laporan siswa
					$query = "SELECT * FROM raport_siswa WHERE laporan='Lulus'";
					$result	= mysqli_query($koneksi, $query);

					$no = 0;
					while ($row = mysqli_fetch_assoc($result)) {
						$no++;
					?>
            		<tr>
                		<td scope="row"><?= $no ?></td>
                		<td scope="row"><?= $row['nama'] ?></td>
                		<td scope="row"><?= $row['kelas'] ?></td>
                        <td scope="row"><?= $row['laporan'];?></td>
					</tr>

					<?php } ?>
				</tbody>
			</table>
        </div>
	</div>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Call to action-->
        <div  id="kegiatan">
        <section class="page-section bg-dark text-white" id="portfolio">
            <div class="container px-4 px-lg-5 text-center">
                <h2 class="mb-4">Kegiatan Kelas Pengayaan</h2>
                <hr class="divider" />
            </div><br><br>
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/pengayaan/a.jpeg" title="Project Name">
                            <img class="img-fluid" src="img/pengayaan/a.jpeg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/pengayaan/b.jpeg" title="Project Name">
                            <img class="img-fluid" src="img/pengayaan/b.jpeg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/pengayaan/c.jpeg" title="Project Name">
                            <img class="img-fluid" src="img/pengayaan/c.jpeg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/pengayaan/d.jpeg" title="Project Name">
                            <img class="img-fluid" src="img/pengayaan/d.jpeg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/pengayaan/e.jpeg" title="Project Name">
                            <img class="img-fluid" src="img/pengayaan/e.jpeg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/pengayaan/f.jpeg" title="Project Name">
                            <img class="img-fluid" src="img/pengayaan/f.jpeg" alt="..." />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <!-- Contact-->
        <section class="page-section" id="daftar">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-lg-8 col-xl-6 text-center">
                        <h2 class="mt-0">Ayo Daftar Sekarang</h2>
                        <hr class="divider" />
                </div>
                <div class="row gx-4 gx-lg-5 justify-content-center mb-5">
                    <div class="col-lg-6">
                        <div class="d-grid"><a class="btn btn-info btn-xl " type="button" href="register.php">Daftar</a></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="bg-light py-5">
            <div class="container px-4 px-lg-5"><div class="small text-center text-muted">Copyright &copy; 2021</div></div>
        </footer>
        <script src="js/scripts.js"></script>

         <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
