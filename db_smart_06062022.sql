/*
 Navicat Premium Data Transfer

 Source Server         : master_mysql
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : db_smart

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 06/06/2022 16:29:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE `kegiatan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `deskripsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kegiatan
-- ----------------------------
INSERT INTO `kegiatan` VALUES (2, 'a.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n');
INSERT INTO `kegiatan` VALUES (3, 'smpn21.png', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n');
INSERT INTO `kegiatan` VALUES (4, 'b.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n');
INSERT INTO `kegiatan` VALUES (5, 'c.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n');
INSERT INTO `kegiatan` VALUES (6, 'd.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n');
INSERT INTO `kegiatan` VALUES (7, 'f.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n');
INSERT INTO `kegiatan` VALUES (8, 'e.jpeg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n');

-- ----------------------------
-- Table structure for kriteria
-- ----------------------------
DROP TABLE IF EXISTS `kriteria`;
CREATE TABLE `kriteria`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kriteria
-- ----------------------------
INSERT INTO `kriteria` VALUES (1, 'Nilai Akademik', '20');
INSERT INTO `kriteria` VALUES (2, 'Nilai Kuis', '30');
INSERT INTO `kriteria` VALUES (3, 'Nilai Agama', '20');
INSERT INTO `kriteria` VALUES (4, 'Sikap Spiritual', '15');
INSERT INTO `kriteria` VALUES (8, 'Sikap Sosial', '15');

-- ----------------------------
-- Table structure for mata_pelajaran
-- ----------------------------
DROP TABLE IF EXISTS `mata_pelajaran`;
CREATE TABLE `mata_pelajaran`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `pelajaran` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mata_pelajaran
-- ----------------------------
INSERT INTO `mata_pelajaran` VALUES (1, 'Pendidikan Agama Islam dan Budi Pekerti');
INSERT INTO `mata_pelajaran` VALUES (2, 'Pendidikan Pancasila dan Kewarganegaraan');
INSERT INTO `mata_pelajaran` VALUES (3, 'Bahasa Indonesia');
INSERT INTO `mata_pelajaran` VALUES (4, 'Matematika (Umum)');
INSERT INTO `mata_pelajaran` VALUES (5, 'Ilmu Pengetahuan Alam (IPA)');
INSERT INTO `mata_pelajaran` VALUES (6, 'Ilmu Pengetahuan Sosial (IPS)');
INSERT INTO `mata_pelajaran` VALUES (7, 'Bahasa Inggris');
INSERT INTO `mata_pelajaran` VALUES (8, 'Seni dan Budaya');
INSERT INTO `mata_pelajaran` VALUES (9, 'Pendidikan Jasmani, Olahraga, dan Kesehatan');
INSERT INTO `mata_pelajaran` VALUES (10, 'Prakarya');
INSERT INTO `mata_pelajaran` VALUES (11, 'PLH');
INSERT INTO `mata_pelajaran` VALUES (12, 'TARTIL');
INSERT INTO `mata_pelajaran` VALUES (13, 'TAMYIZ');
INSERT INTO `mata_pelajaran` VALUES (14, 'TAHFIDZ');
INSERT INTO `mata_pelajaran` VALUES (15, 'Spiritual');
INSERT INTO `mata_pelajaran` VALUES (16, 'Sosial');

-- ----------------------------
-- Table structure for raport_pengayaan
-- ----------------------------
DROP TABLE IF EXISTS `raport_pengayaan`;
CREATE TABLE `raport_pengayaan`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(10) NOT NULL,
  `id_pelajaran` int(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`, `nisn`, `id_pelajaran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of raport_pengayaan
-- ----------------------------
INSERT INTO `raport_pengayaan` VALUES (4, '0094928931', 0, 12);
INSERT INTO `raport_pengayaan` VALUES (5, '0094928931', 0, 13);
INSERT INTO `raport_pengayaan` VALUES (6, '0094928931', 0, 14);

-- ----------------------------
-- Table structure for raport_pengetahuan
-- ----------------------------
DROP TABLE IF EXISTS `raport_pengetahuan`;
CREATE TABLE `raport_pengetahuan`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(10) NOT NULL,
  `id_pelajaran` int(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`, `nisn`) USING BTREE,
  UNIQUE INDEX `id_2`(`id`, `nisn`, `id_pelajaran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 133 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of raport_pengetahuan
-- ----------------------------
INSERT INTO `raport_pengetahuan` VALUES (1, '0094928931', 74, 1);
INSERT INTO `raport_pengetahuan` VALUES (2, '0094928931', 74, 2);
INSERT INTO `raport_pengetahuan` VALUES (3, '0094928931', 78, 3);
INSERT INTO `raport_pengetahuan` VALUES (4, '0094928931', 73, 4);
INSERT INTO `raport_pengetahuan` VALUES (5, '0094928931', 75, 5);
INSERT INTO `raport_pengetahuan` VALUES (6, '0094928931', 71, 6);
INSERT INTO `raport_pengetahuan` VALUES (7, '0094928931', 71, 7);
INSERT INTO `raport_pengetahuan` VALUES (8, '0094928931', 71, 8);
INSERT INTO `raport_pengetahuan` VALUES (9, '0094928931', 81, 9);
INSERT INTO `raport_pengetahuan` VALUES (10, '0094928931', 84, 10);
INSERT INTO `raport_pengetahuan` VALUES (11, '0094928931', 73, 11);
INSERT INTO `raport_pengetahuan` VALUES (12, '0093568860', 83, 1);
INSERT INTO `raport_pengetahuan` VALUES (13, '0093568860', 82, 2);
INSERT INTO `raport_pengetahuan` VALUES (14, '0093568860', 74, 3);
INSERT INTO `raport_pengetahuan` VALUES (15, '0093568860', 76, 4);
INSERT INTO `raport_pengetahuan` VALUES (16, '0093568860', 83, 5);
INSERT INTO `raport_pengetahuan` VALUES (17, '0093568860', 78, 6);
INSERT INTO `raport_pengetahuan` VALUES (18, '0093568860', 77, 7);
INSERT INTO `raport_pengetahuan` VALUES (19, '0093568860', 84, 8);
INSERT INTO `raport_pengetahuan` VALUES (20, '0093568860', 81, 9);
INSERT INTO `raport_pengetahuan` VALUES (21, '0093568860', 84, 10);
INSERT INTO `raport_pengetahuan` VALUES (22, '0093568860', 73, 11);
INSERT INTO `raport_pengetahuan` VALUES (23, ' 0091402417', 95, 1);
INSERT INTO `raport_pengetahuan` VALUES (24, ' 0091402417', 85, 2);
INSERT INTO `raport_pengetahuan` VALUES (25, ' 0091402417', 81, 3);
INSERT INTO `raport_pengetahuan` VALUES (26, ' 0091402417', 90, 4);
INSERT INTO `raport_pengetahuan` VALUES (27, ' 0091402417', 86, 5);
INSERT INTO `raport_pengetahuan` VALUES (28, ' 0091402417', 87, 6);
INSERT INTO `raport_pengetahuan` VALUES (29, ' 0091402417', 94, 7);
INSERT INTO `raport_pengetahuan` VALUES (30, ' 0091402417', 91, 8);
INSERT INTO `raport_pengetahuan` VALUES (31, ' 0091402417', 89, 9);
INSERT INTO `raport_pengetahuan` VALUES (32, ' 0091402417', 90, 10);
INSERT INTO `raport_pengetahuan` VALUES (33, ' 0091402417', 79, 11);
INSERT INTO `raport_pengetahuan` VALUES (34, '0091447016', 95, 1);
INSERT INTO `raport_pengetahuan` VALUES (35, '0091447016', 89, 2);
INSERT INTO `raport_pengetahuan` VALUES (36, '0091447016', 83, 3);
INSERT INTO `raport_pengetahuan` VALUES (37, '0091447016', 93, 4);
INSERT INTO `raport_pengetahuan` VALUES (38, '0091447016', 94, 5);
INSERT INTO `raport_pengetahuan` VALUES (39, '0091447016', 86, 6);
INSERT INTO `raport_pengetahuan` VALUES (40, '0091447016', 92, 7);
INSERT INTO `raport_pengetahuan` VALUES (41, '0091447016', 97, 8);
INSERT INTO `raport_pengetahuan` VALUES (42, '0091447016', 87, 9);
INSERT INTO `raport_pengetahuan` VALUES (43, '0091447016', 92, 10);
INSERT INTO `raport_pengetahuan` VALUES (44, '0091447016', 79, 11);
INSERT INTO `raport_pengetahuan` VALUES (45, '0091800679', 73, 1);
INSERT INTO `raport_pengetahuan` VALUES (46, '0091800679', 74, 2);
INSERT INTO `raport_pengetahuan` VALUES (47, '0091800679', 70, 3);
INSERT INTO `raport_pengetahuan` VALUES (48, '0091800679', 72, 4);
INSERT INTO `raport_pengetahuan` VALUES (49, '0091800679', 78, 5);
INSERT INTO `raport_pengetahuan` VALUES (50, '0091800679', 74, 6);
INSERT INTO `raport_pengetahuan` VALUES (51, '0091800679', 71, 7);
INSERT INTO `raport_pengetahuan` VALUES (52, '0091800679', 73, 8);
INSERT INTO `raport_pengetahuan` VALUES (53, '0091800679', 82, 9);
INSERT INTO `raport_pengetahuan` VALUES (54, '0091800679', 70, 10);
INSERT INTO `raport_pengetahuan` VALUES (55, '0091800679', 72, 11);
INSERT INTO `raport_pengetahuan` VALUES (56, '0085463237', 93, 1);
INSERT INTO `raport_pengetahuan` VALUES (57, '0085463237', 87, 2);
INSERT INTO `raport_pengetahuan` VALUES (58, '0085463237', 84, 3);
INSERT INTO `raport_pengetahuan` VALUES (59, '0085463237', 82, 4);
INSERT INTO `raport_pengetahuan` VALUES (60, '0085463237', 92, 5);
INSERT INTO `raport_pengetahuan` VALUES (61, '0085463237', 85, 6);
INSERT INTO `raport_pengetahuan` VALUES (62, '0085463237', 79, 7);
INSERT INTO `raport_pengetahuan` VALUES (63, '0085463237', 93, 8);
INSERT INTO `raport_pengetahuan` VALUES (64, '0085463237', 89, 9);
INSERT INTO `raport_pengetahuan` VALUES (65, '0085463237', 91, 10);
INSERT INTO `raport_pengetahuan` VALUES (66, '0085463237', 78, 11);
INSERT INTO `raport_pengetahuan` VALUES (67, '0098620740', 88, 1);
INSERT INTO `raport_pengetahuan` VALUES (68, '0098620740', 93, 2);
INSERT INTO `raport_pengetahuan` VALUES (69, '0098620740', 89, 3);
INSERT INTO `raport_pengetahuan` VALUES (70, '0098620740', 87, 4);
INSERT INTO `raport_pengetahuan` VALUES (71, '0098620740', 92, 5);
INSERT INTO `raport_pengetahuan` VALUES (72, '0098620740', 83, 6);
INSERT INTO `raport_pengetahuan` VALUES (73, '0098620740', 86, 7);
INSERT INTO `raport_pengetahuan` VALUES (74, '0098620740', 92, 8);
INSERT INTO `raport_pengetahuan` VALUES (75, '0098620740', 85, 9);
INSERT INTO `raport_pengetahuan` VALUES (76, '0098620740', 92, 10);
INSERT INTO `raport_pengetahuan` VALUES (77, '0098620740', 80, 11);
INSERT INTO `raport_pengetahuan` VALUES (78, ' 0091295466', 81, 1);
INSERT INTO `raport_pengetahuan` VALUES (79, ' 0091295466', 87, 2);
INSERT INTO `raport_pengetahuan` VALUES (80, ' 0091295466', 74, 3);
INSERT INTO `raport_pengetahuan` VALUES (81, ' 0091295466', 87, 4);
INSERT INTO `raport_pengetahuan` VALUES (82, ' 0091295466', 80, 5);
INSERT INTO `raport_pengetahuan` VALUES (83, ' 0091295466', 79, 6);
INSERT INTO `raport_pengetahuan` VALUES (84, ' 0091295466', 79, 7);
INSERT INTO `raport_pengetahuan` VALUES (85, ' 0091295466', 83, 8);
INSERT INTO `raport_pengetahuan` VALUES (86, ' 0091295466', 85, 9);
INSERT INTO `raport_pengetahuan` VALUES (87, ' 0091295466', 85, 10);
INSERT INTO `raport_pengetahuan` VALUES (88, ' 0091295466', 72, 11);
INSERT INTO `raport_pengetahuan` VALUES (89, '0096891223', 95, 1);
INSERT INTO `raport_pengetahuan` VALUES (90, '0096891223', 91, 2);
INSERT INTO `raport_pengetahuan` VALUES (91, '0096891223', 79, 3);
INSERT INTO `raport_pengetahuan` VALUES (92, '0096891223', 88, 4);
INSERT INTO `raport_pengetahuan` VALUES (93, '0096891223', 94, 5);
INSERT INTO `raport_pengetahuan` VALUES (94, '0096891223', 84, 6);
INSERT INTO `raport_pengetahuan` VALUES (95, '0096891223', 92, 7);
INSERT INTO `raport_pengetahuan` VALUES (96, '0096891223', 93, 8);
INSERT INTO `raport_pengetahuan` VALUES (97, '0096891223', 84, 9);
INSERT INTO `raport_pengetahuan` VALUES (98, '0096891223', 88, 10);
INSERT INTO `raport_pengetahuan` VALUES (99, '0096891223', 76, 11);
INSERT INTO `raport_pengetahuan` VALUES (100, ' 0088436615', 78, 1);
INSERT INTO `raport_pengetahuan` VALUES (101, ' 0088436615', 83, 2);
INSERT INTO `raport_pengetahuan` VALUES (102, ' 0088436615', 74, 3);
INSERT INTO `raport_pengetahuan` VALUES (103, ' 0088436615', 82, 4);
INSERT INTO `raport_pengetahuan` VALUES (104, ' 0088436615', 78, 5);
INSERT INTO `raport_pengetahuan` VALUES (105, ' 0088436615', 80, 6);
INSERT INTO `raport_pengetahuan` VALUES (106, ' 0088436615', 79, 7);
INSERT INTO `raport_pengetahuan` VALUES (107, ' 0088436615', 83, 8);
INSERT INTO `raport_pengetahuan` VALUES (108, ' 0088436615', 85, 9);
INSERT INTO `raport_pengetahuan` VALUES (109, ' 0088436615', 80, 10);
INSERT INTO `raport_pengetahuan` VALUES (110, ' 0088436615', 75, 11);

-- ----------------------------
-- Table structure for raport_sikap
-- ----------------------------
DROP TABLE IF EXISTS `raport_sikap`;
CREATE TABLE `raport_sikap`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `predikat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_pelajaran` int(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`, `nisn`) USING BTREE,
  UNIQUE INDEX `id_2`(`id`, `nisn`, `id_pelajaran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of raport_sikap
-- ----------------------------
INSERT INTO `raport_sikap` VALUES (1, '0094928931', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (2, '0094928931', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (3, '0093568860', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (4, '0093568860', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (5, ' 0091402417', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (6, ' 0091402417', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (7, '0091447016', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (8, '0091447016', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (9, '0091800679', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (10, '0091800679', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (11, '0085463237', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (12, '0085463237', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (13, '0098620740', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (14, '0098620740', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (15, ' 0091295466', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (16, ' 0091295466', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (17, '0096891223', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (18, '0096891223', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (19, ' 0088436615', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (20, ' 0088436615', 'Baik', 16);
INSERT INTO `raport_sikap` VALUES (26, '12345', 'Baik', 15);
INSERT INTO `raport_sikap` VALUES (27, '12345', 'Baik', 16);

-- ----------------------------
-- Table structure for raport_siswa
-- ----------------------------
DROP TABLE IF EXISTS `raport_siswa`;
CREATE TABLE `raport_siswa`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nisn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_pelajaran` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_kuis` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `laporan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`, `nisn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of raport_siswa
-- ----------------------------
INSERT INTO `raport_siswa` VALUES (1, 'ABDUL RAHMAN AL RASYID', '7.1', '0094928931', '1 (Satu)', '2021/2022', '80.00', 'Lulus');
INSERT INTO `raport_siswa` VALUES (2, 'ADIIBAH AULIA ARIFIN', '7.1', '0093568860', '1 (Satu)', '2021/2022', '65.00', 'Lulus');
INSERT INTO `raport_siswa` VALUES (3, 'AISYAH ASSHAFFIYAH AL-HAQ', '7.1', ' 0091402417', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (4, 'ANGGUN TYA MAULIDA', '7.1', '0091447016', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (5, 'ANINDYA CARISSA PUTRI', '7.1', '0091800679', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (6, 'AULIA TUZAHRA', '7.1', '0085463237', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (7, 'AZIRA AMANDA PUTRI', '7.1', '0098620740', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (8, 'AZRIL DWI FIRMANSYAH', '7.1', ' 0091295466', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (9, 'BELLA PERTIWI', '7.1', '0096891223', '1 (Satu)', '2021/2022', '0', NULL);
INSERT INTO `raport_siswa` VALUES (10, 'BHANU NATHA ADRIYANTO', '7.1', ' 0088436615', '1 (Satu)', '2021/2022', '0', NULL);

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `title_header` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `lead_header` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `title_tentang_1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `desk_tentang_1` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `title_tentang_2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `desk_tentang_2` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `title_tentang_3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `desk_tentang_3` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `title_informasi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `lead_informasi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `facebook` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `twitter` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `linkedin` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `instagram` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('Sistem Informasi Kelas Unggulan', 'Pentingnya Kamu untuk Mengevaluasi dimana unggulan kalian yang cocok', 'Program Pengayaan', '<p>Beranjak dari tekad dan itikad melahirkan pembisaan mengaji itulah kemulaian Kelas Pengayaan PAI diselenggarakan. Setelah masa dua tahun mencari pola yang ideal sejak diluncurkan sebagai &lsquo;branding proggramme service&rsquo; SMP Negeri 21 kota Tangerang Selatan ini baru di tahun ke-3 model dan konten pembelajarannya mulai bisa didefinifkan. Model kurikulumnya dinamai BASMALAH-Qu yang merupakan akronim dari &lsquo;Biasa Membaca, Menghafal, dan Menerjemah Al-Qur&rsquo;an&rsquo;. Model kurikulum ini dirumuskan bukan tanpa alasan. Selain dorongan profetik agar ikhtiar pembelajaran diarahkan untuk memudahkan peserta didik, pesan-pesan agung dalam Al-Qur&rsquo;an, maupun tausiah profetik yang disampaikan Rasulullah Saw.menyiratkan pesan singkat bahwa berpusat pada Al-Qur&rsquo;an lah pembelajaran inti agama Islam ini dituangkan. Karenanya, pembelajaran paling mendasar yang harus diutamakan kepada peserta didik adalah konten-konten yang didekat-dekatkan dengan pemahaman terhadap Al-Qur&rsquo;an.</p>\r\n', 'Fungsi & Tujuan', '<p>Kelas Pengayaan PAI adalah program pendidikan nonformal yang diselenggarakan pada sekolah umum dalam hal ini SMP Negeri 21 Kota Tangerang Selatan yang berfungsi:</p>\r\n\r\n<ul>\r\n	<li>Memenuhi Kebutuhan Masyarakat akan tambahan pendidikan agama Islam khususnya pendidikan Al-Qur&rsquo;an bagi peserta didik yang belajar di SMP Negeri 21 Kota Tangerang Selatan</li>\r\n	<li>Mempersiapkan Peserta didik menjadi anggota masyarakat yang memahami dan mengamalkan nilai ajaran agama islam.</li>\r\n</ul>\r\n\r\n<p>Adapun Tujuannya adalah Terbentuknya peserta didik yang memahami dan mengamalkan nilai ajaran agama islam dan terbiasa membaca, menghafal dan menerjemahkan Al-Qur&rsquo;an dalam rangka mencerdaskan kehidupan bangsa yang beriman, bertakwa, dan berakhlak mulia</p>\r\n', 'Visi & Misi', '<p>Mencerdaskan kehidupan bangsa yang beriman, bertakwa, dan berakhlak mulia.</p>\r\n', 'Informasi Pengumuman Kelulusan', 'Daftar Informasi Kelulusan.', 'https:///www.facebook.com', 'https:///www.twitter.com', 'https:///www.linkedin.com', 'https:///www.instagram.com');

-- ----------------------------
-- Table structure for soal_kuis
-- ----------------------------
DROP TABLE IF EXISTS `soal_kuis`;
CREATE TABLE `soal_kuis`  (
  `no` int(20) NOT NULL AUTO_INCREMENT,
  `soal` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_a` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_b` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_c` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_d` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kunci_jwb` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`no`) USING BTREE,
  INDEX `kunci_jwb`(`kunci_jwb`(768)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of soal_kuis
-- ----------------------------
INSERT INTO `soal_kuis` VALUES (1, 'Jelaskan arti singkat dari hukum bacaan “Idgham” dalam ilmu tajwid …', 'meleburkan suatu huruf kepada huruf setelahnya', 'jelas', 'pertemuan antara dua huruf yang sama makhrajnya tetapi tidak sama sifatnya', 'Idghom mutaqoribain adalah dua huruf yang berdekatan makhroj dan sifatnya', 'A');
INSERT INTO `soal_kuis` VALUES (2, 'Jika mim mati bertemu dengan huruf ', 'iklab', 'Idgham mitsly', 'Izhar syafawi', 'Ikhfa’ syafawi', 'D');
INSERT INTO `soal_kuis` VALUES (3, '3 hukum bacaan hukum bacaan Mim Sukun adalah ….', 'Ihkfak,iklab,idzhar', 'Mad wajib,mad aridisukun, mad iwat', 'ikhfa syafawi, idgham mimi, dan idzhar syafawi', 'mad ja’is, mad tobi’I, mad badal', 'C');
INSERT INTO `soal_kuis` VALUES (4, 'Ada berapa Asmaul Husna yang terdapat dalam Al-Qur\'an?', '102', '122', '89', '99', 'D');
INSERT INTO `soal_kuis` VALUES (5, 'Salah satu sikap santun dan menghargai orang lain adalah....', 'Suka marah-marah', 'Membantah perkataan orang yang lebih tua', 'Tidak menghina kekurangan orang lain', 'Sombong dan suka mencela', 'C');
INSERT INTO `soal_kuis` VALUES (6, 'Firman Allah SWT yang menjelaskan tentang ditunjukkan nya amaliyahDidunia walau sebesar \"Zarrah\" adalah...', 'Q.S albaqarah: 1-11', 'Q. S Al- Balad 7- 8', 'Q. S  ad-dhuha 8', 'Q. S  az zalzalah 7-8', 'D');
INSERT INTO `soal_kuis` VALUES (7, 'Dibawah ini yang merupakan salah satu manfaat jujur adalah..', 'Mendapat tantangan dari orang lain', 'Hidupnya susah', 'Dipercaya orang lain', 'Mudah diperalat oleh orang lain.', 'C');
INSERT INTO `soal_kuis` VALUES (8, 'Seseorang yang ingkar janji berarti memiliki sikap...', 'Takabur', 'Munafik', 'Hasud', 'Riya', 'B');
INSERT INTO `soal_kuis` VALUES (9, 'Berbakti kepada kedua orang tua dikenal dengan istilah..', 'Sarrul wali dain', 'Wali dain', 'Uququl wali dain', 'Birrul walidain', 'D');
INSERT INTO `soal_kuis` VALUES (10, 'Kewajiban seorang anak ketika orang tuanya meninggal adalah..', 'Mencukupi kebutuhan nya', 'Mendoakannya', 'Memberikan makanan', 'Menggunakan harta warisannya', 'B');
INSERT INTO `soal_kuis` VALUES (11, 'Sikap yang tepat jika orang tua sedang sakit yaitu..', 'Menasehati nya agar tidak sakit', 'Membiarkannya sampai sembuh sendiri', 'Dititipkan dipanti sosial karena sibuk', 'Merawatnya dengan penuh kasih sayang', 'D');
INSERT INTO `soal_kuis` VALUES (12, 'Berdasarkan hadis Nabi Muhammad SAW, kedudukan dan derajat ibu dibanding Bapak adalah..', '3 tingkat dibanding bapak', '4 tingkat dibanding bapak', '5 tingkat dibanding bapak', '6 tingkat dibanding bapak', 'A');
INSERT INTO `soal_kuis` VALUES (13, 'Zakat merupakan kewajiban bagi umat islam. Arti Zakat adalah..', 'Menghapuskan', 'Menyisihkan', 'Menyucikan', 'Memberikan', 'C');
INSERT INTO `soal_kuis` VALUES (14, 'Salah satu jenis zakat adalah zakat mal yaitu..', 'Zakat jiwa', 'Zakat harta', 'Amal jariah', 'Zakat benda', 'B');
INSERT INTO `soal_kuis` VALUES (15, 'qada dan qadar sering disebut dengan sebutan...', 'Ukuran', 'Nasib', 'Takdir', 'Ketentuan', 'C');
INSERT INTO `soal_kuis` VALUES (16, 'Apa arti dari surah Al-fil …', 'Gajah', 'Nabi adam', 'Keluarga imron', 'jamuan', 'A');
INSERT INTO `soal_kuis` VALUES (17, 'Bila hukum bacaan nun sukun atau tanwin jika bertemu dengan huruf B’a di baca …', 'Idzhar', 'Iqlab', 'Ikhfa', 'Idhom bilagunah', 'B');
INSERT INTO `soal_kuis` VALUES (18, 'Berapa jumlah huruf idgam bigunnah’…', '(6). (ya) ? , (ra) ? , (mim) ? , (lam) ? , (wau) ? , (nun) ?', '(1). ( ba’)', '(15)  Ta (?), Tsa (?), Jim (?), Dal (?), Dzal (?), Zay (?), Sin (?), Syin (?), Sod (?), Dhod (?), Tho (?), Dho (?), Fa\' (?), Qof (?), Kaf (?)', '(2) (?), Jim (?)', 'A');
INSERT INTO `soal_kuis` VALUES (19, 'Apa arti dari surah Al-kafirun …', 'Orang-orang kafir', 'Gajah', 'Wanita', 'Jamuan', 'A');
INSERT INTO `soal_kuis` VALUES (20, 'Seorang laki-laki dan seorang perempuan yang hafal Al-qur’an diistilahkan dengan panggilan …', 'Hafidz/hafidoh', 'Ulama', 'Ustad/ustadzah', 'wali', 'A');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('user','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`, `nisn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '1234', 'admin', '$2y$10$kFvmjcGo3reoTfOcPYm0Ee/U.jy7y5d/RlvKUxdKK.DTMgbMxrpUu', 'admin');
INSERT INTO `user` VALUES (5, '0094928931', 'ABDUL RAHMAN AL RASYID', '$2y$10$NSsBFDhbjRMIbfuxfrfMSeE15aJDv0qaVSMoVEHxcaUJEKtz6Lyci', 'user');
INSERT INTO `user` VALUES (6, '0093568860', 'ADIIBAH AULIA ARIFIN', '$2y$10$WCwIkuCbf/5pZuZt1IlVVOC7AtEjsBvYe59Jli6H2AXzxe.qRMu4u', 'user');
INSERT INTO `user` VALUES (7, '02145454', 'eby', '$2y$10$wBAB9GHvzuwFn//e3iYFI.Kd5.HS3lyyVgipKeU3ZmPwLzMZVZaS2', 'user');
INSERT INTO `user` VALUES (8, '0098620740', 'azira', '$2y$10$LF6yC.QZlqxDVgiNG81Ba.DfGnqA/0xHm3mNiIIcI7jVfu1z1f5HW', 'user');
INSERT INTO `user` VALUES (9, '123123', '123123', '$2y$10$9Nir9.yiBz2F1LU274YofuAIIKfYFIlKgjTKfxdjEV7Cd/I1UMHba', 'user');

SET FOREIGN_KEY_CHECKS = 1;
