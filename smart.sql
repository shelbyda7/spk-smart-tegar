-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2022 at 09:00 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart`
--

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int(20) NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `nama`, `bobot`) VALUES
(1, 'Nilai Akademik', '20'),
(2, 'Nilai Kuis', '30'),
(3, 'Nilai Agama', '20'),
(4, 'Sikap Spiritual', '15'),
(8, 'Sikap Sosial', '15');

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id` int(50) NOT NULL,
  `pelajaran` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `pelajaran`) VALUES
(1, 'Pendidikan Agama Islam dan Budi Pekerti'),
(2, 'Pendidikan Pancasila dan Kewarganegaraan'),
(3, 'Bahasa Indonesia'),
(4, 'Matematika (Umum)'),
(5, 'Ilmu Pengetahuan Alam (IPA)'),
(6, 'Ilmu Pengetahuan Sosial (IPS)'),
(7, 'Bahasa Inggris'),
(8, 'Seni dan Budaya'),
(9, 'Pendidikan Jasmani, Olahraga, dan Kesehatan'),
(10, 'Prakarya'),
(11, 'PLH'),
(12, 'TARTIL'),
(13, 'TAMYIZ'),
(14, 'TAHFIDZ'),
(15, 'Spiritual'),
(16, 'Sosial');

-- --------------------------------------------------------

--
-- Table structure for table `raport_pengayaan`
--

CREATE TABLE `raport_pengayaan` (
  `id` int(10) NOT NULL,
  `nisn` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(10) NOT NULL,
  `id_pelajaran` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `raport_pengayaan`
--

INSERT INTO `raport_pengayaan` (`id`, `nisn`, `nilai`, `id_pelajaran`) VALUES
(4, '0094928931', 0, 12),
(5, '0094928931', 0, 13),
(6, '0094928931', 0, 14);

-- --------------------------------------------------------

--
-- Table structure for table `raport_pengetahuan`
--

CREATE TABLE `raport_pengetahuan` (
  `id` int(10) NOT NULL,
  `nisn` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(10) NOT NULL,
  `id_pelajaran` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `raport_pengetahuan`
--

INSERT INTO `raport_pengetahuan` (`id`, `nisn`, `nilai`, `id_pelajaran`) VALUES
(1, '0094928931', 74, 1),
(2, '0094928931', 74, 2),
(3, '0094928931', 78, 3),
(4, '0094928931', 73, 4),
(5, '0094928931', 75, 5),
(6, '0094928931', 71, 6),
(7, '0094928931', 71, 7),
(8, '0094928931', 71, 8),
(9, '0094928931', 81, 9),
(10, '0094928931', 84, 10),
(11, '0094928931', 73, 11),
(12, '0093568860', 83, 1),
(13, '0093568860', 82, 2),
(14, '0093568860', 74, 3),
(15, '0093568860', 76, 4),
(16, '0093568860', 83, 5),
(17, '0093568860', 78, 6),
(18, '0093568860', 77, 7),
(19, '0093568860', 84, 8),
(20, '0093568860', 81, 9),
(21, '0093568860', 84, 10),
(22, '0093568860', 73, 11),
(23, ' 0091402417', 95, 1),
(24, ' 0091402417', 85, 2),
(25, ' 0091402417', 81, 3),
(26, ' 0091402417', 90, 4),
(27, ' 0091402417', 86, 5),
(28, ' 0091402417', 87, 6),
(29, ' 0091402417', 94, 7),
(30, ' 0091402417', 91, 8),
(31, ' 0091402417', 89, 9),
(32, ' 0091402417', 90, 10),
(33, ' 0091402417', 79, 11),
(34, '0091447016', 95, 1),
(35, '0091447016', 89, 2),
(36, '0091447016', 83, 3),
(37, '0091447016', 93, 4),
(38, '0091447016', 94, 5),
(39, '0091447016', 86, 6),
(40, '0091447016', 92, 7),
(41, '0091447016', 97, 8),
(42, '0091447016', 87, 9),
(43, '0091447016', 92, 10),
(44, '0091447016', 79, 11),
(45, '0091800679', 73, 1),
(46, '0091800679', 74, 2),
(47, '0091800679', 70, 3),
(48, '0091800679', 72, 4),
(49, '0091800679', 78, 5),
(50, '0091800679', 74, 6),
(51, '0091800679', 71, 7),
(52, '0091800679', 73, 8),
(53, '0091800679', 82, 9),
(54, '0091800679', 70, 10),
(55, '0091800679', 72, 11),
(56, '0085463237', 93, 1),
(57, '0085463237', 87, 2),
(58, '0085463237', 84, 3),
(59, '0085463237', 82, 4),
(60, '0085463237', 92, 5),
(61, '0085463237', 85, 6),
(62, '0085463237', 79, 7),
(63, '0085463237', 93, 8),
(64, '0085463237', 89, 9),
(65, '0085463237', 91, 10),
(66, '0085463237', 78, 11),
(67, '0098620740', 88, 1),
(68, '0098620740', 93, 2),
(69, '0098620740', 89, 3),
(70, '0098620740', 87, 4),
(71, '0098620740', 92, 5),
(72, '0098620740', 83, 6),
(73, '0098620740', 86, 7),
(74, '0098620740', 92, 8),
(75, '0098620740', 85, 9),
(76, '0098620740', 92, 10),
(77, '0098620740', 80, 11),
(78, ' 0091295466', 81, 1),
(79, ' 0091295466', 87, 2),
(80, ' 0091295466', 74, 3),
(81, ' 0091295466', 87, 4),
(82, ' 0091295466', 80, 5),
(83, ' 0091295466', 79, 6),
(84, ' 0091295466', 79, 7),
(85, ' 0091295466', 83, 8),
(86, ' 0091295466', 85, 9),
(87, ' 0091295466', 85, 10),
(88, ' 0091295466', 72, 11),
(89, '0096891223', 95, 1),
(90, '0096891223', 91, 2),
(91, '0096891223', 79, 3),
(92, '0096891223', 88, 4),
(93, '0096891223', 94, 5),
(94, '0096891223', 84, 6),
(95, '0096891223', 92, 7),
(96, '0096891223', 93, 8),
(97, '0096891223', 84, 9),
(98, '0096891223', 88, 10),
(99, '0096891223', 76, 11),
(100, ' 0088436615', 78, 1),
(101, ' 0088436615', 83, 2),
(102, ' 0088436615', 74, 3),
(103, ' 0088436615', 82, 4),
(104, ' 0088436615', 78, 5),
(105, ' 0088436615', 80, 6),
(106, ' 0088436615', 79, 7),
(107, ' 0088436615', 83, 8),
(108, ' 0088436615', 85, 9),
(109, ' 0088436615', 80, 10),
(110, ' 0088436615', 75, 11);

-- --------------------------------------------------------

--
-- Table structure for table `raport_sikap`
--

CREATE TABLE `raport_sikap` (
  `id` int(10) NOT NULL,
  `nisn` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `predikat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_pelajaran` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `raport_sikap`
--

INSERT INTO `raport_sikap` (`id`, `nisn`, `predikat`, `id_pelajaran`) VALUES
(1, '0094928931', 'Baik', 15),
(2, '0094928931', 'Baik', 16),
(3, '0093568860', 'Baik', 15),
(4, '0093568860', 'Baik', 16),
(5, ' 0091402417', 'Baik', 15),
(6, ' 0091402417', 'Baik', 16),
(7, '0091447016', 'Baik', 15),
(8, '0091447016', 'Baik', 16),
(9, '0091800679', 'Baik', 15),
(10, '0091800679', 'Baik', 16),
(11, '0085463237', 'Baik', 15),
(12, '0085463237', 'Baik', 16),
(13, '0098620740', 'Baik', 15),
(14, '0098620740', 'Baik', 16),
(15, ' 0091295466', 'Baik', 15),
(16, ' 0091295466', 'Baik', 16),
(17, '0096891223', 'Baik', 15),
(18, '0096891223', 'Baik', 16),
(19, ' 0088436615', 'Baik', 15),
(20, ' 0088436615', 'Baik', 16),
(26, '12345', 'Baik', 15),
(27, '12345', 'Baik', 16);

-- --------------------------------------------------------

--
-- Table structure for table `raport_siswa`
--

CREATE TABLE `raport_siswa` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nisn` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_pelajaran` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_kuis` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `laporan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `raport_siswa`
--

INSERT INTO `raport_siswa` (`id`, `nama`, `kelas`, `nisn`, `semester`, `tahun_pelajaran`, `nilai_kuis`, `laporan`) VALUES
(1, 'ABDUL RAHMAN AL RASYID', '7.1', '0094928931', '1 (Satu)', '2021/2022', '80.00', 'Lulus'),
(2, 'ADIIBAH AULIA ARIFIN', '7.1', '0093568860', '1 (Satu)', '2021/2022', '65.00', 'Lulus'),
(3, 'AISYAH ASSHAFFIYAH AL-HAQ', '7.1', ' 0091402417', '1 (Satu)', '2021/2022', '0', NULL),
(4, 'ANGGUN TYA MAULIDA', '7.1', '0091447016', '1 (Satu)', '2021/2022', '0', NULL),
(5, 'ANINDYA CARISSA PUTRI', '7.1', '0091800679', '1 (Satu)', '2021/2022', '0', NULL),
(6, 'AULIA TUZAHRA', '7.1', '0085463237', '1 (Satu)', '2021/2022', '0', NULL),
(7, 'AZIRA AMANDA PUTRI', '7.1', '0098620740', '1 (Satu)', '2021/2022', '0', NULL),
(8, 'AZRIL DWI FIRMANSYAH', '7.1', ' 0091295466', '1 (Satu)', '2021/2022', '0', NULL),
(9, 'BELLA PERTIWI', '7.1', '0096891223', '1 (Satu)', '2021/2022', '0', NULL),
(10, 'BHANU NATHA ADRIYANTO', '7.1', ' 0088436615', '1 (Satu)', '2021/2022', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `soal_kuis`
--

CREATE TABLE `soal_kuis` (
  `no` int(20) NOT NULL,
  `soal` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_a` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_b` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_c` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwb_d` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kunci_jwb` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `soal_kuis`
--

INSERT INTO `soal_kuis` (`no`, `soal`, `jwb_a`, `jwb_b`, `jwb_c`, `jwb_d`, `kunci_jwb`) VALUES
(1, 'Jelaskan arti singkat dari hukum bacaan “Idgham” dalam ilmu tajwid …', 'meleburkan suatu huruf kepada huruf setelahnya', 'jelas', 'pertemuan antara dua huruf yang sama makhrajnya tetapi tidak sama sifatnya', 'Idghom mutaqoribain adalah dua huruf yang berdekatan makhroj dan sifatnya', 'A'),
(2, 'Jika mim mati bertemu dengan huruf ', 'iklab', 'Idgham mitsly', 'Izhar syafawi', 'Ikhfa’ syafawi', 'D'),
(3, '3 hukum bacaan hukum bacaan Mim Sukun adalah ….', 'Ihkfak,iklab,idzhar', 'Mad wajib,mad aridisukun, mad iwat', 'ikhfa syafawi, idgham mimi, dan idzhar syafawi', 'mad ja’is, mad tobi’I, mad badal', 'C'),
(4, 'Ada berapa Asmaul Husna yang terdapat dalam Al-Qur\'an?', '102', '122', '89', '99', 'D'),
(5, 'Salah satu sikap santun dan menghargai orang lain adalah....', 'Suka marah-marah', 'Membantah perkataan orang yang lebih tua', 'Tidak menghina kekurangan orang lain', 'Sombong dan suka mencela', 'C'),
(6, 'Firman Allah SWT yang menjelaskan tentang ditunjukkan nya amaliyahDidunia walau sebesar \"Zarrah\" adalah...', 'Q.S albaqarah: 1-11', 'Q. S Al- Balad 7- 8', 'Q. S  ad-dhuha 8', 'Q. S  az zalzalah 7-8', 'D'),
(7, 'Dibawah ini yang merupakan salah satu manfaat jujur adalah..', 'Mendapat tantangan dari orang lain', 'Hidupnya susah', 'Dipercaya orang lain', 'Mudah diperalat oleh orang lain.', 'C'),
(8, 'Seseorang yang ingkar janji berarti memiliki sikap...', 'Takabur', 'Munafik', 'Hasud', 'Riya', 'B'),
(9, 'Berbakti kepada kedua orang tua dikenal dengan istilah..', 'Sarrul wali dain', 'Wali dain', 'Uququl wali dain', 'Birrul walidain', 'D'),
(10, 'Kewajiban seorang anak ketika orang tuanya meninggal adalah..', 'Mencukupi kebutuhan nya', 'Mendoakannya', 'Memberikan makanan', 'Menggunakan harta warisannya', 'B'),
(11, 'Sikap yang tepat jika orang tua sedang sakit yaitu..', 'Menasehati nya agar tidak sakit', 'Membiarkannya sampai sembuh sendiri', 'Dititipkan dipanti sosial karena sibuk', 'Merawatnya dengan penuh kasih sayang', 'D'),
(12, 'Berdasarkan hadis Nabi Muhammad SAW, kedudukan dan derajat ibu dibanding Bapak adalah..', '3 tingkat dibanding bapak', '4 tingkat dibanding bapak', '5 tingkat dibanding bapak', '6 tingkat dibanding bapak', 'A'),
(13, 'Zakat merupakan kewajiban bagi umat islam. Arti Zakat adalah..', 'Menghapuskan', 'Menyisihkan', 'Menyucikan', 'Memberikan', 'C'),
(14, 'Salah satu jenis zakat adalah zakat mal yaitu..', 'Zakat jiwa', 'Zakat harta', 'Amal jariah', 'Zakat benda', 'B'),
(15, 'qada dan qadar sering disebut dengan sebutan...', 'Ukuran', 'Nasib', 'Takdir', 'Ketentuan', 'C'),
(16, 'Apa arti dari surah Al-fil …', 'Gajah', 'Nabi adam', 'Keluarga imron', 'jamuan', 'A'),
(17, 'Bila hukum bacaan nun sukun atau tanwin jika bertemu dengan huruf B’a di baca …', 'Idzhar', 'Iqlab', 'Ikhfa', 'Idhom bilagunah', 'B'),
(18, 'Berapa jumlah huruf idgam bigunnah’…', '(6). (ya) ? , (ra) ? , (mim) ? , (lam) ? , (wau) ? , (nun) ?', '(1). ( ba’)', '(15)  Ta (?), Tsa (?), Jim (?), Dal (?), Dzal (?), Zay (?), Sin (?), Syin (?), Sod (?), Dhod (?), Tho (?), Dho (?), Fa\' (?), Qof (?), Kaf (?)', '(2) (?), Jim (?)', 'A'),
(19, 'Apa arti dari surah Al-kafirun …', 'Orang-orang kafir', 'Gajah', 'Wanita', 'Jamuan', 'A'),
(20, 'Seorang laki-laki dan seorang perempuan yang hafal Al-qur’an diistilahkan dengan panggilan …', 'Hafidz/hafidoh', 'Ulama', 'Ustad/ustadzah', 'wali', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(20) NOT NULL,
  `nisn` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nisn`, `username`, `pass`, `role`) VALUES
(1, '1234', 'admin', '$2y$10$PLUl4GiLjHKdBsFopJtsRu//OVAshwEdMM/J/78wtG0rliMWZiLAy', 'admin'),
(5, '0094928931', 'ABDUL RAHMAN AL RASYID', '$2y$10$NSsBFDhbjRMIbfuxfrfMSeE15aJDv0qaVSMoVEHxcaUJEKtz6Lyci', 'user'),
(6, '0093568860', 'ADIIBAH AULIA ARIFIN', '$2y$10$WCwIkuCbf/5pZuZt1IlVVOC7AtEjsBvYe59Jli6H2AXzxe.qRMu4u', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raport_pengayaan`
--
ALTER TABLE `raport_pengayaan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`nisn`,`id_pelajaran`);

--
-- Indexes for table `raport_pengetahuan`
--
ALTER TABLE `raport_pengetahuan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`nisn`),
  ADD UNIQUE KEY `id_2` (`id`,`nisn`,`id_pelajaran`);

--
-- Indexes for table `raport_sikap`
--
ALTER TABLE `raport_sikap`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`nisn`),
  ADD UNIQUE KEY `id_2` (`id`,`nisn`,`id_pelajaran`);

--
-- Indexes for table `raport_siswa`
--
ALTER TABLE `raport_siswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`nisn`);

--
-- Indexes for table `soal_kuis`
--
ALTER TABLE `soal_kuis`
  ADD PRIMARY KEY (`no`),
  ADD KEY `kunci_jwb` (`kunci_jwb`(768));

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`nisn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `raport_pengayaan`
--
ALTER TABLE `raport_pengayaan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `raport_pengetahuan`
--
ALTER TABLE `raport_pengetahuan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `raport_sikap`
--
ALTER TABLE `raport_sikap`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `raport_siswa`
--
ALTER TABLE `raport_siswa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `soal_kuis`
--
ALTER TABLE `soal_kuis`
  MODIFY `no` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
